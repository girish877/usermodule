package org.gsp.userlogin.service.impl;

import org.gsp.userlogin.beans.User;
import org.gsp.userlogin.dao.UserDao;
import org.gsp.userlogin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService
{
	@Autowired
	UserDao userDao;

	public User registerUser(User user)
	{
		return userDao.save(user);
	}

	public String validateUser(String username, String password)
	{

		User user = userDao.findOne(username);
		if (user.getUsername().equals(username) && user.getPassword().equals(password))
		{
			return "valid user";
		}
		else
		{
			return "user not valid";
		}

	}

	public boolean removeUser(String username)
	{
		userDao.delete(username);
		return true;
	}
	
	public User updateUser(User user){
		return userDao.save(user);
	}
	
	public Iterable<User> getAllUsers(){
		return userDao.findAll();
	}

}
