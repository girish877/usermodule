package org.gsp.userlogin.service;

import org.gsp.userlogin.beans.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService
{
	public User registerUser(User user);
	public String validateUser(String username, String password);
	public boolean removeUser(String username);
	public User updateUser(User user);
	public Iterable<User> getAllUsers();

}
