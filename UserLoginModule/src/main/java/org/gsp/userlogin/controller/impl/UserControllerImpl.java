package org.gsp.userlogin.controller.impl;
import org.gsp.userlogin.beans.User;
import org.gsp.userlogin.controller.UserController;
import org.gsp.userlogin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserControllerImpl implements UserController{
	
	@Autowired
	UserService userService;

    public String validateUser(String username, String password){
    	return userService.validateUser(username, password);
    }
    
    
    public User registerUser(User user){
    	return userService.registerUser(user);
    }
    
    public boolean removeUser(String username){
    	return userService.removeUser(username);
    }
    
    public Iterable<User> getAllUsers(){
    	return userService.getAllUsers();
    }
    
    public User updateUser(User user){
    	return userService.updateUser(user);
    }
   
}
