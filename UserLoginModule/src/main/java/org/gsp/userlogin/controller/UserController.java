package org.gsp.userlogin.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gsp.userlogin.beans.User;
import org.springframework.stereotype.Component;

@Component
@Path("/user")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_ATOM_XML , MediaType.APPLICATION_JSON})
public interface UserController
{
	@Path("/validate/user/{username}/pwd/{password}")
	@GET
	public String validateUser(@PathParam("username") String username,@PathParam("password") String password);
	
	@Path("/registerUser")
	@POST
	public User registerUser(User user);

	@Path("/removeUser/{username}")
	@DELETE
	public boolean removeUser(@PathParam("username") String username);

	@Path("/allUser")
	@GET
	public Iterable<User> getAllUsers();

	@Path("/updateUser")
	@PUT
	public User updateUser(User user);

}
