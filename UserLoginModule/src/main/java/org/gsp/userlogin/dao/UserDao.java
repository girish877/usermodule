package org.gsp.userlogin.dao;

import org.gsp.userlogin.beans.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, String>
{

}
